from diagrams import Cluster, Diagram
from diagrams.generic.device import Mobile
from diagrams.saas.chat import Telegram
from diagrams.programming.language import Python
from diagrams.generic.os import Ubuntu
from diagrams.custom import Custom

with Diagram("Hashcrack Diagram", show=False):
    user = Mobile("User")
    frontend = Telegram("Front-End Bot")
    backend = Python("Back-End Bot")

    with Cluster("Crack Flows"):
        with Cluster("Crack workers"):
            api_hash = Custom("Search-That-Hash", "./resources/apiicon.png")
            hashcat = Custom("Hashcat", "./resources/hashcatjpg.jpg")

        core = Ubuntu("Core")

    user >> frontend >> backend >> hashcat >> core
    backend >> api_hash
