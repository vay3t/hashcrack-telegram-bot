![](media/banner_pro_muito_pro.png)


# Hashcrack Telegram Bot
Telegram Bot for Hashcrack (Crack without SSH or NAT), for WSL or Linux.

# Presentation in Bsides Chile 2021

* https://youtu.be/YwhioxWlUWo?t=24918
    - https://gitlab.com/vay3t/Presentaciones/-/tree/main/BsidesCL

## Diagram of project

![](diagram-bot/hashcrack_diagram.png)

# Options

```
/help - Show this help
/install <hashcat/wordlist/gendb/all> - Install hashcat, wordlist or generate db in server
/info - Show info about hashcat and utils

/fastcrack <HASH> - Crack hash online

### Max 20MB files
/addhashfile <UPLOAD-HASHFILE> - Upload hash file
/addwordlist <UPLOAD-WORDLISTFILE> - Upload wordlist
/addmask <UPLOAD-MASKFILE> - Upload file masks
/addrules <UPLOAD-RULESFILE> - Upload file rules
###

/identify <UPLOAD-HASHFILE> - Identify hash in upload
/identify <HASH> - Identify hash in message

/gethashlist - Download hash list
/benchmark <IDHASH> - Benchmark of hash
/grephashid <STRING> - Grep hash id list

/hashcat <HASHID> <HASHFILESTORED> <WORDLIST> - Fast crack usage
/status - Status of actual job
/potfiles - Show all passwords cracked
/listsessions - List all sessions
/killall confirm - Kill actual job

/wget <URL> - Download wordlist from url (Disk Limit)

# not work /restore <SESSIONNAME> - Restore session
# not work /clearsessions confirm - Delete all sessions
# not work /clearpotfile - Delete all passwords cracked
# not work /wipe all - Wipe all files [NOT IMPLEMENTED]
```

# Install

```bash
sudo apt install -y python3-pip python3-dev p7zip-full
git clone https://gitlab.com/vay3t/hashcrack-telegram-bot
cd hashcrack-telegram-bot
sudo pip3 install -r requirements.txt

# Add your bot token and user id in .env file, check .env.example. In TOKEN_BOT and LIST_OF_ADMINS, replace the token with your own and add your own admins ID.
echo 'TOKEN_BOT="999999999:AAAAAAAAAAAAAAAAAAAAAAAAAAA"' > .env
echo 'LIST_OF_ADMINS="11111111111,22222222222222,33333333333333333"' >> .env
```

## How to create token bot
* https://core.telegram.org/bots#6-botfather
* https://www.youtube.com/watch?v=L5lLcXH8h44

# Run
Only run in dir of hashcrack-telegram-bot

```bash
python3 main.py
```

## First steps

### Check privileges and run
First you need to check that you have authorization to use the bot

```
/start
```

If you answer `Hi!` It means that everything is fine. Otherwise it will not respond, this means that you do not have authorization and you need to modify the `.env` file or the bot has a problem, if the bot has a problem you will know it in the execution logs.

### Install all
To make it short use the following command.

```
/install all
```

#### Fast run
To crack a hash online, you will need to specify the following:

```
/fastcrack 21232f297a57a5a743894a0e4a801fc3
```

#### Fast identify hash

```bash
/identify 21232f297a57a5a743894a0e4a801fc3
```

### Upload hash file
To add a hash file, you will need to upload a file and in the message before sending it specify the following:

```
/addhashfile
```

### Run Hashcat
To make your first crack as an example for an MD5 hash write the following:

```
/hashcat 0 hashes/hash.md5 wordlists/rockyou.txt 
```

And if you are anxious, to see the status use:

```
/status
```

It is necessary to take into account that the status is updated every 1 minute approx.

### When I finish
Gone! Now you just need to log in with the cracked password.


# Run in Google Colab
Doc in dev https://gitlab.com/vay3t/hashcrack-telegram-bot/-/blob/main/colabcat-telegram.ipynb

# Project structure

```
.
├── LICENSE
├── README.md
├── colabcat-telegram.ipynb
├── core.py
├── create_env.py
├── diagram-bot
│   ├── diagram-bot.py
│   ├── hashcrack_diagram.png
│   └── resources
│       ├── apiicon.png
│       ├── colab.png
│       └── hashcatjpg.jpg
├── hashcat-*
│   ├── hashcat.bin
│   ├── hashes.db
│   └── (...)
├── hashes
│   ├── md5.hash
│   ├── md5.test
│   └── sha512.hash
├── lib.py
├── main.py
├── masks
├── media
│   ├── 400x400_auto_x1.png
│   ├── banner_pro_muito_pro.png
│   └── hashcracklogo.png
├── outputs
│   └── hashcat.output
├── potfiles
│   ├── hashcat.potfile
│   └── hashcat_01.potfile
├── requirements.txt
├── rules
├── sessions
├── tmp
└── wordlists
    └── rockyou.txt
```

# Use cases
* To borrow the neighbor's internet. Don't be serious, it's a joke.

# TODO
- [ ] Add documentation and examples of use of this bot (Gif files)
- [X] Add custom url for hashcat download (zip file) -- Edit: Only lst or txt
- [ ] add option `/wipe all <PASSWD>` with password
- [X] Add doc of run with Google Colab
- [ ] Fix bug when upload file with name with space
- [X] Fix bug of limit of upload file
- [ ] Porting to golang -- Maybe not


# Notes
* Tested in Python 3.8.10 on Ubuntu 20.04 (WSL)
* Coded with autopep8 standard: https://github.com/hhatto/autopep8
* Has not done QA testing, so it is not guaranteed to work.
* If you have any questions or problems to make the bot work, contact me.
* If you want to contribute to the development of this bot, please contact me.

# Contact
https://twitter.com/vay3t
